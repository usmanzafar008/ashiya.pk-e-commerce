<?php
include('header.php');
include('db.php');
if (empty($_SESSION['id'])) {
    die("<h2>Login to View</h2>");
}
?>

<h1>View Inventory</h1>

<div id="navigationbar">
    <table width="100%" border="1%">
        <tr>
            <td width="16.6%"><b>Name</b></td>
            <td width="16.6%"><b>Quantity</b></td>
            <td width="16.6%"><b>Price</b></td>
            <td width="16.6%"><b>Edit</b></td>
            <td width="16.6%"><b>Delete</b></td>
            <td width="16.6%"><b>upload image</b></td>
        </tr>
        <tbody>

            <?php
            $sql = "SELECT * FROM inventory";

            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                
                while($row = $result->fetch_assoc())
                {
                    echo "<tr>
                        <td>".$row['name']."</td>
                        <td>".$row['qty']."</td>
                        <td>".$row['price']."</td>
                        <td><a href='edit.php?id=".$row['id']."'>Edit</a></td>
                        <td><a href='delete.php?id=".$row['id']."'>Delete</a></td>
                        <td><a href='file.php?id=".$row['id']."'>image</a></td>
                    
                    </tr>";
                }
            }


            ?>

        </tbody>

    </table>
</div>
<div id="footer">
    <table width="100%" border="1%">

        <td><b>©Copyrights</b><br><b>-All right Reserved </b></td>

    </table>
</div>